package com.zuitt.discussion;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;

@SpringBootApplication
//This application will function as an endpoint that will be used in handling http request
@RestController
// Will require all routes within the class to use the set endpoint as part of its route.
@RequestMapping("/greeting")
public class DiscussionApplication {

	private ArrayList<Student> students = new ArrayList<Student>();
	private ArrayList<String> enrollees = new ArrayList<String>();
	public static void main(String[] args) {
		SpringApplication.run(DiscussionApplication.class, args);
	}


//	localhost:8080/hello

	@GetMapping("/hello")
//	Maps a get request to the route "/hello" and invokes the method hello().
	public String hello(){
		return "Hello World";
	}
//	Route with String Query
// 	local:8080/hi?name=value
	@GetMapping("/hi")
//	"@RequestParam" annotation that allows us to extract data from query strings in the URL
	public String hi(@RequestParam(value="name", defaultValue = "John") String name) {
		return String.format("Hi %s", name);

	}

//	Multiple Parameters
//	localhost:8080/friend?name=value&friend=value
	@GetMapping("/friend")
	public String friend(@RequestParam(value="name", defaultValue = "Joe") String name, @RequestParam(value="friend", defaultValue = "Jane") String friend){
		return	String.format("Hello %s! My name is %s", friend, name);
	}


//	Route with path variables
//	Dynamic data is obtained directly from the url
//	localhost:8080/name

	@GetMapping("/hello/{name}")
// "@PathVariable" annotation allows us to extract data directly from the URL
	public String greetFriend(@PathVariable("name") String name){
		return String.format("Nice to meet you %s!", name);

	}

// Activity for S09

//	localhost:8080/greeting/enroll?user=
	@GetMapping("/enroll")
	public String enroll(@RequestParam(value = "user") String user){
	enrollees.add(user);
	return "Thank you for enrolling, "+ user +"!";
}

//	localhost:8080/greeting/getEnrollees
	@GetMapping("/getEnrollees")
	public String enroll(){
		return enrollees.toString();
	}

//	localhost:8080/greeting/nameage?=[name]&age=[age]
	@GetMapping("/nameage")
	public String nameAge(@RequestParam(value = "name") String name,
						  @RequestParam(value = "age") String age){
		return "Hello "+ name + "! My age is "+ age;
	}

//	localhost:8080/greeting/courses/[id]
	@GetMapping("/courses/{id}")
	public String courses(@PathVariable("id") String id){
		switch (id){
			case "java101":
				return "Name: Java 101, Schedule: MWF 8:00 AM-11:00 AM, Price: PHP 3000.00";
			case "sql101":
				return "Name: SQL 101, Schedule: TTH 1:00 PM-4;00 PM, Price: PHP 2000.00";
			case "javaee101":
				return "Name: Java EE 101, Schedule: MWF 1:00 PM-4:00 PM, Price: PHP 3500.00";
			default:
				return "Course cannot be found!";
		}
	}

}



